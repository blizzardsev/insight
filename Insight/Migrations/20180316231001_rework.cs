﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Insight.Migrations
{
    public partial class rework : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                "SubscriptionID",
                "Skill");

            migrationBuilder.DropColumn(
                name: "SubscriptionID",
                table: "Skill");
           
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
        }
    }
}
