﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Insight.Migrations
{
    public partial class FUCKYOU : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Skill_Subscription_SubscriptionID",
                table: "Skill");

            migrationBuilder.DropForeignKey(
                name: "FK_User_Subscription_ID",
                table: "User");

            migrationBuilder.DropTable(
                name: "Subscription");

            migrationBuilder.DropIndex(
                name: "IX_Skill_SubscriptionID",
                table: "Skill");

            migrationBuilder.DropColumn(
                name: "SubscriptionID",
                table: "Skill");

            migrationBuilder.AlterColumn<int>(
                name: "ID",
                table: "User",
                type: "int",
                nullable: false,
                oldClrType: typeof(int))
                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "ID",
                table: "User",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int")
                .OldAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            migrationBuilder.AddColumn<int>(
                name: "SubscriptionID",
                table: "Skill",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Subscription",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Subscription", x => x.ID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Skill_SubscriptionID",
                table: "Skill",
                column: "SubscriptionID");

            migrationBuilder.AddForeignKey(
                name: "FK_Skill_Subscription_SubscriptionID",
                table: "Skill",
                column: "SubscriptionID",
                principalTable: "Subscription",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_User_Subscription_ID",
                table: "User",
                column: "ID",
                principalTable: "Subscription",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
