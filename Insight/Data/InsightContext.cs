﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Insight.Models;

namespace Insight.Data
{
    //Database context for InSight application
    public class InsightContext : DbContext
    {
        public InsightContext(DbContextOptions<InsightContext> options) : base(options)
        {

        }

        public DbSet<Feedback> Feedback { get; set; }
        public DbSet<Message> Message { get; set; }
        public DbSet<Request> Request { get; set; }
        public DbSet<Skill> Skill { get; set; }
        public DbSet<Topic> Topic { get; set; }
        public DbSet<TopicItem> TopicItem { get; set; }
        public DbSet<User> User { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Feedback>().ToTable("Feedback");
            modelBuilder.Entity<Message>().ToTable("Message");
            modelBuilder.Entity<Request>().ToTable("Request");
            modelBuilder.Entity<Skill>().ToTable("Skill");
            modelBuilder.Entity<Topic>().ToTable("Topic");
            modelBuilder.Entity<TopicItem>().ToTable("TopicItem");
            modelBuilder.Entity<User>().ToTable("User");

            modelBuilder.Entity<Message>().HasOne(u => u.Sender).WithMany(u => u.UserSentMessages);
            modelBuilder.Entity<Message>().HasOne(u => u.Receiver).WithMany(u => u.UserReceivedMessages);

            modelBuilder.Entity<Skill>()
                .HasMany(i => i.SkillTopics).WithOne(j => j.ParentSkill)
                .HasForeignKey(j => j.ParentSkillID);

            modelBuilder.Entity<Topic>()
                .HasMany(i => i.TopicContent).WithOne(j => j.ParentTopic)
                .HasForeignKey(j => j.ParentTopicID);
        }
    }
}
