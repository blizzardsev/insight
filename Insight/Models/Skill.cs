﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Insight.Models
{
    /// <summary>
    /// Master item residing over a range of Topics.
    /// </summary>
    public class Skill
    {
        public int ID { get; set; }
        public String SkillName { get; set; }
        public DateTime LastUpdated { get; set; }
        public String SkillImage { get; set; }

        public ICollection<Topic> SkillTopics { get; set; }
        public User Creator { get; set; }
    }
}
