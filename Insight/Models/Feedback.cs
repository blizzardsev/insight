﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Insight.Models
{
    /// <summary>
    /// Item for a piece of feedback relating to site content; used for rating, gathering user input.
    /// </summary>
    public class Feedback
    {
        public int ID { get; set; }
        public String Content { get; set; }
        public int RatingScore { get; set; }
        public DateTime SubmissionDate { get; set; }

        public User Sender { get; set; }
    }
}
