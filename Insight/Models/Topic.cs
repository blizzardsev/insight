﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Insight.Models
{
    /// <summary>
    /// Master item covering a particular topic; or subset of a Skill
    /// </summary>
    public class Topic
    {
        public int ID { get; set; }
        public String TopicName { get; set; }
        public DateTime LastUpdated { get; set; }

        public int ParentSkillID { get; set; }
        public Skill ParentSkill { get; set; }
        public ICollection<TopicItem> TopicContent { get; set; }
        public User Creator { get; set; }
    }
}
