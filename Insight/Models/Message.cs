﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Insight.Models
{
    /// <summary>
    /// Holds message information; used to inform users on a one-to-one/one-to-many basis.
    /// NOT intended for user-to-user use; restrict to admin-to-user/system-to-user only.
    /// </summary>
    public class Message
    {
        public int ID { get; set; }
        public String Content { get; set; }
        public DateTime SendDate { get; set; }
        public DateTime ReadDate { get; set; }
        public bool IsSystemMessage { get; set; }
        public bool IsVisible { get; set; }
        public bool IsExpiring { get; set; }

        public User Sender { get; set; }
        public User Receiver { get; set; }
    }
}
