﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Insight.Models;

namespace Insight.Models
{
    /// <summary>
    /// User class; self explanatory.
    /// </summary>
    public class User
    {
        public int ID { get; set; }
        public String WindowsLogin { get; set; }
        public String UserName { get; set; }
        public String PassHash { get; set; }
        public String Avatar { get; set; }
        public DateTime LastActive { get; set; }
        public int AccessLevel { get; set; }

        public ICollection<Request> UserRequests { get; set; }
        public ICollection<Message> UserSentMessages { get; set; }
        public ICollection<Message> UserReceivedMessages { get; set; }
        public ICollection<Feedback> UserFeedback { get; set; }
    }
}
