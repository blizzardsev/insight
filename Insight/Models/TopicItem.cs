﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Insight.Models
{
    /// <summary>
    /// Class to hold a set of information for a particular Topic; a list of these will make up a Topic's content.
    /// Text or Image based.
    /// </summary>
    public class TopicItem
    {
        public int ID { get; set; }
        public String Content { get; set; }
        public String ItemType { get; set; }
        public int OrderNumber { get; set; }
        public DateTime CreationDate { get; set; }
        public int ParentTopicID { get; set; }

        public Topic ParentTopic { get; set; }
    }
}
