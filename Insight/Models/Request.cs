﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Insight.Models
{
    /// <summary>
    /// Item for a change/correction request regarding a piece of content.
    /// </summary>
    public class Request
    {
        public int ID { get; set; }
        public String Content { get; set; }
        public bool IsSeen { get; set; }
        public bool IsActioned { get; set; }

        public User Sender { get; set; }
        public Topic TargetTopic { get; set; }
        public Skill TargetSkill { get; set; }
    }
}
