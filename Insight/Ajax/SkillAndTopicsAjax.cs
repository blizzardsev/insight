﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Insight.Models;

namespace Insight.Ajax
{
    public class SkillAndTopicsAjax
    {
        public String skillName;
        public String skillLastUpdate;
        public List<TopicData> skillTopics = new List<TopicData>();

        public class TopicData
        {
            public int topicID;
            public String topicName;

            public TopicData(Topic inputTopic)
            {
                topicID = inputTopic.ID;
                topicName = inputTopic.TopicName;
            }
        }

        public SkillAndTopicsAjax(Skill inputSkill)
        {
            skillName = inputSkill.SkillName;
            skillLastUpdate = String.Format("{0} | {1}", inputSkill.LastUpdated.ToShortDateString(), inputSkill.LastUpdated.ToShortTimeString());
            
            foreach(Topic thisTopic in inputSkill.SkillTopics)
            {
                skillTopics.Add(new TopicData(thisTopic));
            }
        }
    }
}
