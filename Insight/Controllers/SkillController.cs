﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Insight.Models;
using Insight.Data;
using Insight.Ajax;
using Newtonsoft.Json;

namespace Insight.Controllers
{
    public class SkillController : Controller
    {
        private readonly InsightContext _context;

        //Constructor
        public SkillController(InsightContext context)
        {
            //Get DB context
            _context = context;
        }

        /// <summary>
        /// Return the Skill listing page.
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Index()
        {
            List<Skill> allSkills = await _context.Skill
                .Include(i => i.SkillTopics)
                .Include(i => i.Creator)
                .ToListAsync();
            
            ViewBag.openingLines = new string[] {
                "Go on, you know you want to.",
                "Arm yourself with knowledge.",
                "Delve into the depths.",
                "We're sure you'll find something useful.",
                "Browse to your heart's content.",
                "Please - we insist." };
            return View(allSkills);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="skillID"></param>
        /// <returns></returns>
        public async Task<JsonResult> GetTopicAjax(int skillID)
        {
            Skill thisSkill = await _context.Skill
                .Include(i => i.SkillTopics)
                .Where(i => i.ID == skillID)
                .SingleAsync();
            SkillAndTopicsAjax returnData = new SkillAndTopicsAjax(thisSkill);

            return Json(returnData);
        }
    }
}