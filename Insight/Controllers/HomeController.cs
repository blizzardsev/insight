﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Insight.Models;
using Insight.Data;

namespace Insight.Controllers
{
    public class HomeController : Controller
    {
        private readonly InsightContext _context;

        //Constructor
        public HomeController(InsightContext context)
        {
            //Get DB context
            _context = context;
        }

        /// <summary>
        /// Return the InSight home page.
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Index()
        {
            //Get three skills at random for featured skills
            var allSkills = await _context.Skill.ToListAsync();
            List<Skill> featuredSkills = new List<Skill>();
            Random randGen = new Random();

            for (int i = 0; i < 3; i++)
            {
                int randSkillIndex = randGen.Next(0, allSkills.Count());
                featuredSkills.Add(allSkills.ElementAt(randGen.Next(0, allSkills.Count())));
                allSkills.RemoveAt(randSkillIndex);
            }
            ViewBag.FeaturedSkills = featuredSkills;

            return View();
        }
    }
}
