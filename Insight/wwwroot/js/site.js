﻿$(document).ready(function () {

    //Modify time-dependant elements to match day state
    if (new Date().getHours() > 18) {
        $('#skills_eggTimer').attr('src', '/images/Site/timer_night.png');
    }
    else if (new Date().getHours() < 8) {
        $('#skills_eggTimer').attr('src', '/images/Site/timer_morning.png');
    }
    $('.st_rotatingAnim').css('-webkit-animation', 'rotation ' + (1 + (new Date().getHours() / 5)) + 's infinite linear');
});

//Show the main site menu
function ShowMenu() {

    if ($('#site_Menu').attr('data-isClosed') == 1) {
        $('#site_Navbar').addClass('selected');
        $('#site_Menu').fadeIn(150);
        setTimeout(function () { $('#site_Menu').attr('data-isClosed', 0); }, 160);
    }
}

//Hide the main site menu
function HideMenu() {

    if ($('#site_Menu').attr('data-isClosed') == 0) {
        $('#site_Navbar').removeClass('selected');
        $('#site_Menu').fadeOut(150);
        setTimeout(function () { $('#site_Menu').attr('data-isClosed', 1); }, 160);
    }
}

//Show a given menu option
function SwitchMenu() {
    if ($('#site_Menu').attr('data-isClosed') == 1) {
        ShowMenu();
    }
    else {
        HideMenu();
    }
}
