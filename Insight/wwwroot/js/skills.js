﻿$(document).ready(function () {

});

//Subscribe to a specific Skill
function SubscribeTo() {
    $.ajax({
        url: 'SkillController/GetTopicAjax',
        success: function () {

        },
        complete: function () {

        },
        error: function () {

        }
    });
}

//Unsubscribe from a specific skill
function UnsubscribeTo() {
    $.ajax({
        url: 'SkillController/GetTopicAjax',
        success: function () {

        },
        complete: function () {

        },
        error: function () {

        }
    });
}

//View the topics associated with a specific skill
function SkillPreview(caller) {

    //Only fire if not currently in process
    if (!$('#skills_eggTimer').hasClass('active')) {

        //Alter visuals
        $('.skills_skillItem').removeClass('selected');
        $('#skills_topicInfo').hide();
        $('#skills_topicScroller').hide();
        $('.skills_topicItem').remove();

        setTimeout(function () {
            $('#skills_eggTimer').addClass('active');
            $('#skills_eggTimer').fadeIn(100);
        }, 205);

        $(caller).addClass('selected');

        //Get the ID of the Skill to retrieve data for
        var skillID = $(caller).attr('data-skillID');

        //Fetch the newly selected Skill's data from the Controller
        setTimeout(function () {
            $.ajax({
                type: 'GET',
                url: '/Skill/GetTopicAjax',
                data: {
                    'skillID': skillID
                },
                //Successful call
                success: function (data) {
                    $('#skills_eggTimer').fadeOut(100);
                    $('#skills_topicTitle').text(data.skillName);
                    $('#skills_topicLastUpdate').text('Updated: ' + data.skillLastUpdate);

                    for (var i = 0; i < data.skillTopics.length; i++) {
                        $('#skills_topicScroller').append(
                            '<div class="skills_topicItem">' + data.skillTopics[i].topicName + '</div>'
                        );
                    }

                    setTimeout(function () {
                        $('#skills_eggTimer').removeClass('active');
                        $('#skills_topicInfo').show();
                        $('#skills_topicScroller').show();
                    }, 205);
                },
                //Call complete
                complete: function () {
                },
                //Failed call
                error: function (xhr, status, error) {
                    console.log('Skills: AJAX call failed!');
                    var err = JSON.parse(xhr.responseText);
                    alert(err.Message);
                }
            });
        }, 500);
    }
}