﻿$(document).ready(function () {
    $('.home_FeaturedItem').eq(0).addClass('selected');

    setTimeout(function () { SwitchFeaturedItem(); }, 10000);
});

function SwitchFeaturedItem() {
    var currentActiveFeatured = $('.home_FeaturedItem.selected').eq(0);
    var nextRefToShow = parseInt($(currentActiveFeatured).attr('data-featuredItemRef')) + 1;
    if (nextRefToShow == 2) {
        nextRefToShow = 0;
    }
    
    $(currentActiveFeatured).fadeOut(200);
    setTimeout(function () {
        $(currentActiveFeatured).removeClass('selected');
        
        setTimeout(function () {
            $('.home_FeaturedItem[data-featuredItemRef="' + nextRefToShow + '"]').fadeIn(200);
            $('.home_FeaturedItem[data-featuredItemRef="' + nextRefToShow + '"]').addClass('selected');
        });
    }, 200);
    setTimeout(function () { SwitchFeaturedItem(); }, 10000);

}
